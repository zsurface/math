\documentclass[UTF8]{article}
% Update Tue Dec 18 12:55:04 2018 
% Move include head to aronlib.tex
% Add aronlib.tex
\input{aronlib.tex}
\usepackage{tikz}
\usetikzlibrary{tikzmark}
%
\newlength{\mytextsize}
\makeatletter
      \setlength{\mytextsize}{\f@size pt}
\makeatother
\newcommand{\where}{\quad \tc{yellow}{\text{where}} \quad}
\newcommand{\lhs}{\tc{green}{\text{ LHS }}}
\newcommand{\rhs}{\tc{yellow}{\text{ RHS }}}
\newcommand{\bec}{\quad \because \quad}
%
\newcommand{\JoinUp}[5]{\begin{tikzpicture}[remember picture,overlay,line width=0.05\mytextsize]
    \draw([shift={(#1\mytextsize,#2\mytextsize)}]pic cs:start#5) -- ++(0pt,0.7\mytextsize) -| ([shift={(#3\mytextsize,#4\mytextsize)}]pic cs:end#5);
    \end{tikzpicture}}
\newcommand{\JoinDown}[5]{\begin{tikzpicture}[remember picture,overlay,line width=0.05\mytextsize]
    \draw([shift={(#1\mytextsize,#2\mytextsize)}]pic cs:start#5) -- ++(0pt,-0.7\mytextsize) -| ([shift={(#3\mytextsize,#4\mytextsize)}]pic cs:end#5);
    \end{tikzpicture}}
%

% 
\begin{document}
\pagecolor{gray}
\color{green}
%
\section{Mathematic Induction}
If $n$ is positive integer, then \\
% eqation label, eqation tags
\begin{align} \label{eq:1}
    \frac{1}{1 \cdot 2} + \frac{1} { 2 \cdot 3} + \cdots + \frac{1} { n \cdot (n + 1)} = \frac{n} {n + 1}
\end{align}
\begin{proof}
  Check the basic cases: Let $ n = 1, 2$ \\
  \begin{align*}
    \text{ LHS } &= \frac{1} { 1 \cdot 2} \\
    \tc{red}{\text{ RHS }} &= \frac{n} { n \cdot (n + 1)} = \frac{1} {1 \cdot 2} = \frac{1} {2} \\
    \text{ LHS } &= \tc{red}{\text{ RHS }} \where n = 1 \\
    %
    \text{ LHS } &= \frac{1} { 1 \cdot 2} + \frac{1} { 2 \cdot 3} = \frac{3}{6} + \frac{1}{6} = \frac{2}{3} \\
    \tc{red}{\text{ RHS }} &= \frac{n} { (n + 1)} = \frac{2} {3} \\
    \text{ LHS } &= \tc{red}{\text{ RHS }} \where n = 2 \\
    %
    \frac{1}{1 \cdot 2} &= \frac{n} {n + 1} = \frac{1}{2} \where n = 1 \\
    \frac{1} {1 \cdot 2} + \frac{2}{ 2 \cdot 3}  &= \frac{3}{6} + \frac{2}{6} = \frac{5}{6} \where n = 2\\
  \end{align*}
  \begin{align*}
\shortintertext{If we assume $ n = k$ then the statement is true. We have following}
    \lhs &= \frac{1}{ 1 \cdot 2} + \frac{1} {2 \cdot 3} + \cdots + \frac{1}{k \cdot (k + 1)} \\
    \rhs &= \frac{k} { k + 1 } \\
    \lhs &= \rhs \qquad \tags{(1)}\\
  \end{align*}
  %
  \begin{align*}
\shortintertext{In inductive step, if $ n = k + 1$ then we have following}
    \lhs &= \frac{1}{ 1 \cdot 2} + \frac{1} {2 \cdot 3} + \cdots + \frac{1}{k \cdot (k + 1)} + \frac{1}{(k+1) \cdot (k + 2)} \\
    \lhs &= \frac{k} {k + 1} + \frac{1}{(k+1) \cdot (k + 2)} \quad \bec  \tags{(1)}\\
    \lhs &= \frac{k(k + 2)}{(k+1)(k+2)} + \frac{1}{(k+1) \cdot (k + 2)} \\
    \lhs &= \frac{k(k + 2) + 1} {(k+1) \cdot (k + 2)} = \frac{(k+1)^2}{(k+1)(k+2)} = \tc{pink}{\frac{k+1}{k+2}} \\
    \rhs &= \tc{pink}{\frac{k+1} {k+2}} \\
    \lhs &= \rhs \\
    % KEY: eqation reference
    \implies ~& \eqref{eq:1} \text{ is true for all positive $n$}
  \end{align*}
  %
\end{proof}
\begin{proof}
  \begin{align*}
  % https://tex.stackexchange.com/questions/54587/vertical-spacing-within-align-environment-accounting-for-fractions
  % KEY: connect term, link term, underline, underbrace
  \frac{1}{1 \cdot 2} + \frac{1} { 2 \cdot 3} + \cdots + \frac{1} { n \cdot (n + 1)} &=
   (\frac{1}{1} -  \tikzmark{starta}{\frac{1}{2}}) +  (\tikzmark{enda}{\frac{1}{2}} - \tikzmark{startb}{\frac{1}{3}}) + (\tikzmark{endb}{\frac{1}{3}} -  \tikzmark{startc}{\frac{1}{4}}) + \tikzmark{startd}{\tikzmark{endc}{\cdots}} + (\tikzmark{endd}{\frac{1}{n}} - \frac{1}{n+1})   \\[2.0ex]
     &= 1 - \frac{1}{n+1} = \frac{n}{n+1} \\
    \implies & \eqref{eq:1} \text{ is true for all positive $n$}
               \JoinDown{0.5}{-0.9}{0.5}{-0.9}{a}
               \JoinDown{0.5}{-0.9}{0.5}{-0.9}{b}
               \JoinDown{0.5}{-0.9}{0.5}{-0.9}{c}
               \JoinDown{0.7}{-0.9}{0.5}{-0.9}{d}
  \end{align*}
  %
\end{proof}
%
%
\end{document}
