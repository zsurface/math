\documentclass[UTF8]{article}
% Update Tue Dec 18 12:55:04 2018 
% Move include head to aronlib.tex
% Add aronlib.tex
\input{aronlib.tex}
\begin{document}
\pagecolor{gray}
\color{green}
% KEY: Japanese Mathematical Olympaid 2004 question Q2
%
%
\begin{problem}[Japanese Mathematical Olympaid 2004 question Q]
  Solve for $f(x)$
  \begin{align*}
    f : & \mathbf{R} \rightarrow \mathbf{R} \\
    f( x f(x) + f(y)) &= f(x)^2 + y \where x, y \in \mathbf{R}
  \end{align*}
\end{problem}
\begin{example}[Test some special functions]
  \begin{align*}
    \shortintertext{let $\tc{pink}{f(x) = x}$ and $ x = y$}
    \lhs &= f(x^2 + x)  \\
    \rhs &= x^2 + x  \\
    \Rightarrow f(x) &= x \quad \text{is one of the solution.} \\
    %
    \shortintertext{let $f(x) = a x$ and $ x = y$} \\
    \lhs &= f(a x^2 + a x) = a (a x^2 + ax) = a^2 x^2 + a^2 x \\
    \rhs &= a^2 x^2 + x \\
    \text{ if } \lhs &= \rhs \text{ then } a^2 = 1 \\
    \Rightarrow a &= \pm 1 \\
    %
    \shortintertext{let $x = y$} \\
    \lhs &= f(x f(x) + f(x)) = f(f(x)(x + 1)) \\
    \rhs &= f(x)^2 + x \\
    \shortintertext{let $ \tc{pink}{f(x) = ax^2 + b}$ and $x = y$} \\
    f(x( f(x) + f(y)) &= f (x (ax^2 + b) + ax^2 + b) = [x (ax^2 + b) + ax^2 + b)]^2 \\
      \Rightarrow \deg{\lhs} &= 6 \\
    f(x)^2 + y &= (ax^2 + b)^2 + x \\
    \Rightarrow \deg{\rhs} &= 4 \\
    \Rightarrow \lhs &\neq  \rhs \\
  \end{align*}
\end{example}
\end{document}
