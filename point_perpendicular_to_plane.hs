% Update Tue Dec 18 12:55:04 2018 
% Move include head to aronlib.tex
% Add aronlib.tex
\input{aronlib.tex}
\begin{document}
\section{Compute the point perpendicular to plane} 
Given a points: $p_0$ and three points $q_0, q_1, q2 \in S$  \\
Compute a point $p_n$ on the plane and line $q_0, p_n$ is perpendicular to the plane \\
    \begin{equation}
    \begin{aligned}
    v_0 &= q_0 - p_0   
    \end{aligned}
    \end{equation} 
\end{document}
