(TeX-add-style-hook
 "Try44"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "UTF8")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("asymptote" "inline")))
   (TeX-run-style-hooks
    "latex2e"
    "aronlib"
    "article"
    "art10"
    "pagecolor"
    "lipsum"
    "amsmath"
    "amsfonts"
    "amssymb"
    "amsthm"
    "centernot"
    "tikz"
    "xcolor"
    "fullpage"
    "asymptote")
   (LaTeX-add-amsthm-newtheorems
    "theorem"
    "defintion"
    "collorary"
    "example"
    "remark"
    "note"))
 :latex)

