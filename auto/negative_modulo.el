(TeX-add-style-hook
 "negative_modulo"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "UTF8")))
   (TeX-run-style-hooks
    "latex2e"
    "aronlib"
    "article"
    "art10"))
 :latex)

