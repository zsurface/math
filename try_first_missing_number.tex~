\documentclass[preview]{standalone}
\usepackage{tikz}
\input{aronlib.tex}
\newcommand{\it}{\textit}
% \newgeometry{vmargin={1mm}, hmargin={1mm,1mm}}   % set the margins
\begin{document}
%
Find the first missing positive integer from a list \\
\begin{example}
  Given a list \{1, 2, 5, 4\} , the \tc{red}{First} missing positive integer is \tc{red}{3}
\end{example}
\begin{example}
  Given a list \{2, 3, 5\} , the \tc{red}{First} missing positive integer is \tc{red}{1}
\end{example}
%
\begin{example}
  Given a list \{1, 3, 2, 5\} \\
  \text{let } n \in \{1, 3, 2, 5\} \\
  % \text{if } n - 1 \text{ is NOT inside the range of indices:} \{0, 1, 2, 3\}
\end{example}
\begin{center}
  \begin{tabular}{|c|l|l|l|l|} \hline
    input &     &                                &              &                             \\ \hline
    1    & [0] &  \it{1-1} \in \{0, 1, 2, 3\}   & [1-1] = 1,   &                          \\
    3    & [1] &  \tc{red}{\it{3-1}}  \in \{0, 1, 2, 3\} & \tc{brown}{[2-1]} = 2  &         \\
    2    & [2] &  \tc{brown}{\it{2-1}}  \in \{0, 1, 2, 3\} & \tc{red}{[3-1]} = 3      &             \\
    5    & [3] &  \it{5-1} \notin \{0, 1, 2, 3\}  & [i=3] = -5  &   \leftarrow  \\
  \end{tabular} \hline
\end{center}
%
\begin{center}
\begin{tabular}{|c|l|l|l|l|} \hline
  input &     &                                &              &                             \\ \hline
  1     & [0] &  \it{1-1} \in \{0, 1, 2, 3\}   & [1-1] = 1,   &                 \\
  6     & [1] &  \tc{red}{\it{6-1}}  \notin \{0, 1, 2, 3\} & [i=1] = -6, 2  &        \\
  6     & [2] &  \tc{brown}{\it{6-1}}  \notin \{0, 1, 2, 3\} & [i=2] = -6      &  \leftarrow              \\
  2     & [3] &  \it{2-1} \in \{0, 1, 2, 3\}  & [i=3] = -2  &     \\
\end{tabular} \hline
\end{center}
%
\begin{center}
\begin{tabular}{|c|l|l|l|l|} \hline
  input &     &                                &              &                             \\ \hline
  1     & [0] &  \it{1-1} \in \{0, 1, 2, 3\}   & [1-1] = 1,   &                 \\
  6     & [1] &  \tc{red}{\it{6-1}}  \notin \{0, 1, 2, 3\} & [i=1] = -6  &  \leftarrow       \\
  6     & [2] &  \tc{brown}{\it{6-1}}  \notin \{0, 1, 2, 3\} & \tc{green}{[3-1]} = -6, 3      &             \\
  3     & [3] &  \tc{green}{\it{3-1}} \in \{0, 1, 2, 3\}  & [i=3] = -3  &     \\
\end{tabular} \hline
\end{center}
\begin{center}
\begin{tabular}{|c|l|l|l|l|} \hline
  input &     &                                &              &                             \\ \hline
  1     & [0] &  \it{1-1} \in \{0, 1, 2, 3\}   & [1-1] = 1,   &                 \\
  6     & [1] &  \tc{red}{\it{6-1}}  \notin \{0, 1, 2, 3\} & [i=1] = -6  &  \leftarrow       \\
  6     & [2] &  \tc{brown}{\it{6-1}}  \notin \{0, 1, 2, 3\} & \tc{green}{[3-1]} = -6, 3      &             \\
  3     & [3] &  \tc{green}{\it{3-1}} \in \{0, 1, 2, 3\}  & [i=3] = -3  &     \\
\end{tabular} \hline
\end{center}
\begin{center}
\begin{tabular}{|c|l|l|l|l|} \hline
  input &     &                                &              &                             \\ \hline
  1     & [0] &  \it{1-1} \in \{0, 1, 2, 3\}   & [1-1] = 1,   &                 \\
  6     & [1] &  \tc{red}{\it{6-1}}  \notin \{0, 1, 2, 3\} & [i=1] = -6  &  \leftarrow       \\
  6     & [2] &  \tc{brown}{\it{6-1}}  \notin \{0, 1, 2, 3\} & \tc{green}{[3-1]} = -6, 3      &             \\
  3     & [3] &  \tc{green}{\it{3-1}} \in \{0, 1, 2, 3\}  & [i=3] = -3  &     \\
\end{tabular} \hline
\end{center}
\end{document}
