% Update Wed Jul 18 11:03:24 PDT 2018
% Add aronlib.tex
\documentclass[UTF8]{article}
\usepackage{pagecolor,lipsum}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{centernot}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{fullpage}
\usepackage[inline]{asymptote}
\usepackage{listings}
\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri}
\newtheorem{theorem}{Theorem}
\newtheorem{defintion}{Definition}
\newtheorem{collorary}{Collorary}
\newtheorem{example}{Example}
\newtheorem{remark}{Remark}
\newtheorem{note}{Note} 
\input{aronlib}
% no indentation
\setlength\parindent{0pt}

% Derive projection matrix from u to v
\begin{document}
Given $u, v$,  $u$ projects onto $v$,
$w = \sigma v$ where $\sigma$ is the scala 
we have $v \perp (w - u)$ \\
\begin{align*}
    v \perp (\sigma v - u)                  \\ 
    v^{T} (\sigma v - u)  = 0               \\ 
    \sigma v^{T} v - v^{T} u = 0                \\ 
   \sigma v^{T} v = v^{T} u                \\ 
    \sigma = \frac{v^{T}u}{v^{T}v}          \\  \\
   \sigma  v=  \frac{ v v^{T} u } {v^{T} v} \\ 
    \sigma v =  \frac{ v v^{T}} {v^{T} v} u \\   
\end{align*}
$vv^T$ is the projection matrix which projects $u$ onto $v$ \\

Concret Example:
Let's project $ 
u = \left[ \begin{array}{c}
1 \\
1
\end{array} \right]$ onto 
$ v = \left[ \begin{array}{c}
0 \\
1
\end{array} \right]$ \\ 
$ vv^{T} = 
\left[ \begin{array}{c}
0 \\
1
\end{array} \right] 
\left[ \begin{array}{cc}
0 & 1 
\end{array} \right] 
= \begin{bmatrix}
0 & 0\\
0 & 1 
\end{bmatrix}  \\
Proj_v = 
\begin{bmatrix}
0 & 0\\
0 & 1 
\end{bmatrix}
\left[ \begin{array}{c}
1 \\
1
\end{array} \right] 
=
\left[ \begin{array}{c}
0 \\
1
\end{array} \right] 
$





\end{document}
