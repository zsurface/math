% Update Wed Jul 18 11:03:24 PDT 2018
% Add aronlib.tex
\documentclass[UTF8]{article}
\usepackage{pagecolor,lipsum}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{centernot}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{fullpage}
\usepackage[inline]{asymptote}
\usepackage{listings}
\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri}
\newtheorem{theorem}{Theorem}
\newtheorem{defintion}{Definition}
\newtheorem{collorary}{Collorary}
\newtheorem{example}{Example}
\newtheorem{remark}{Remark}
\newtheorem{note}{Note} 
\input{aronlib}
% no indentation
\setlength\parindent{0pt}
\begin{document}
How to reduce Row Echelon Form, the determinant is changed here
Three Axioms for determinant function: \newline
1. Exchange two different rows $\det{A} = (-1)\det{A}$\newline
2. Multiply one row with non-zero scalar $\det{A} = n\det{A}$ \newline
3. Multiply one row with non-zero scalar and add it to other row $\det{A} = \det{A}$ \newline
\begin{flushleft}
    $A = \begin{bmatrix}
    1 & 2 & 3 \\
    4 & 5 & 6\\
    7 & 8 & 10 
    \end{bmatrix} 
    \Rightarrow
    B = \begin{bmatrix}
    1 & 2 & 3\\
    1 & 2 & 3\\
    \end{bmatrix}$ 
    %
    \newline
\text{duplicated $n - 1$ copy of first row} 
\begin{verbatim}
init $ map(\x -> head x) A 
\end{verbatim} 
\end{flushleft}
%
\begin{flushleft}
    $A = \begin{bmatrix}
    1 & 2 & 3 \\
    4 & 5 & 6\\
    7 & 8 & 10 
    \end{bmatrix} 
    \Rightarrow
    C = \begin{bmatrix}
    4 & 5 & 6\\
    7 & 8 & 10 
    \end{bmatrix}$  
    %
    \newline
\end{flushleft}
\text{remove first row} 
\begin{verbatim}
tail A
\end{verbatim} 
%
\begin{flushleft}
     $\begin{bmatrix}
     4(1 & 2 & 3) \\
     7(1 & 2 & 3)
     \end{bmatrix}
     -
     \begin{bmatrix}
     1(4 & 5 & 6) \\
     1(7 & 8 & 10) 
     \end{bmatrix}  
     \Rightarrow D = \begin{bmatrix}
     0 & 3 & 6 \\
     0 & 9 & 11 
     \end{bmatrix}$  
\end{flushleft}
Zero the first column 
\begin{flushleft}
\begin{verbatim}
zipWith(\rx ry -> 
                let 
                    xx = head rx 
                    yy = head ry 
                in 
                    if xx == 0 || yy == 0 
                    then ry 
                    else zipWith(\x y -> yy*x - xx*y) rx ry 
            ) m1 m2
\end{verbatim} 
\end{flushleft}
% 
%
\begin{verbatim}
map(\x -> tail x) D 
\end{verbatim} 
%
%
\begin{flushleft}
     $\begin{bmatrix}
     3 & 6 \\
     9 & 11 
     \end{bmatrix}  
     \Rightarrow
     \begin{bmatrix}
     3 & 6 
     \end{bmatrix} \newline
     \begin{bmatrix}
     3 & 6 \\
     9 & 11 
     \end{bmatrix}  
     \Rightarrow
     \begin{bmatrix}
     9 & 11 
     \end{bmatrix} \newline
     %
     \begin{bmatrix}
     9(3 & 6) 
     \end{bmatrix}  
     -
     \begin{bmatrix}
     3(9 & 11) 
     \end{bmatrix}  
     =
     \begin{bmatrix}
     0 & 21 
     \end{bmatrix}$  
\end{flushleft}
Final matrix
\begin{flushleft}
    $\begin{bmatrix}
    1 & 2 & 3\\
    0 & 3 & 6\\
    0 & 0 & 21 
    \end{bmatrix}$ 
\end{flushleft}

%
\end{document}
