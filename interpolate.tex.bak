% Update Wed Jul 18 11:03:24 PDT 2018
% Add aronlib.tex
\documentclass[UTF8]{article}
\usepackage{pagecolor,lipsum}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{centernot}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{fullpage}
\usepackage[inline]{asymptote}
\usepackage{listings}
\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri}
\newtheorem{theorem}{Theorem}
\newtheorem{defintion}{Definition}
\newtheorem{collorary}{Collorary}
\newtheorem{example}{Example}
\newtheorem{remark}{Remark}
\newtheorem{note}{Note} 
\input{aronlib}
% no indentation
\setlength\parindent{0pt}

\begin{document}
Linear interpolation uses linear function to find the a weight of point between given two points.
Given two point $A = (x_1, y_2)$ and $B = (x_2, y_2)$
Find a weight of a point between $A$ and $B$
    \begin{equation}
    \begin{aligned}
            x' &= (1 - t)x_1 + t x_2 \\
            y' &= (1 - t)y_1 + t y_2  \quad \text{ where } 0 \leq t \leq 1 \\
    \end{aligned}
    \end{equation} 

Given point: $(p_{0}, p_{1}, p_{2}, p_{3})$
from $p_0$ to $p_1$ compute $p_{01}$
    \begin{equation}
    \begin{aligned}
            x_{01} = (1 - t)x_0 + t x_1 \\
            y_{01} = (1 - t)y_0 + t y_1  \\
    \end{aligned}
    \end{equation} 
from $p_2$ to $p_3$ compute $p_{23}$
    \begin{equation}
    \begin{aligned}
            x_{23} = (1 - t)x_2 + t x_3 \\
            y_{23} = (1 - t)y_2 + t y_3  \\
    \end{aligned}
    \end{equation} 
from $p_{01}, p_{23}$ compute $p_{0123}$ 
    \begin{equation}
    \begin{aligned}
            x_{0123} = (1 - t)x_{01} + t x_{23} \\
            y_{0123} = (1 - t)y_{01} + t y_{23}  \\
    \end{aligned}
    \end{equation} 

\end{document}
