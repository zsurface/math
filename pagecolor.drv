%%
%% This is file `pagecolor.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% pagecolor.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% Project: pagecolor
%% Version: 2017/05/29 v1.0i
%% 
%% Copyright (C) 2011 - 2017 by
%%     H.-Martin M"unch <Martin dot Muench at Uni-Bonn dot de>
%% 
%% The usual disclaimer applies:
%% If it doesn't work right that's your problem.
%% (Nevertheless, send an e-mail to the maintainer
%%  when you find an error in this package.)
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3c of this license or (at your option) any later
%% version. This version of this license is in
%%    http://www.latex-project.org/lppl/lppl-1-3c.txt
%% and the latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status "maintained".
%% 
%% The Current Maintainer of this work is H.-Martin Muench.
%% 
%% This work consists of the main source file pagecolor.dtx,
%% the README, and the derived files
%%    pagecolor.sty, pagecolor.pdf,
%%    pagecolor.ins, pagecolor.drv,
%%    pagecolor-example.tex, pagecolor-example.pdf.
%% 
%% In memoriam
%%  Claudia Simone Barth + 1996/01/30
%%  Tommy Muench + 2014/01/02
%%  Hans-Klaus Muench + 2014/08/24
%% 
\NeedsTeXFormat{LaTeX2e}[2014/05/01]
\ProvidesFile{pagecolor.drv}%
  [2017/05/29 v1.0i Provides thepagecolor (HMM)]
\documentclass{ltxdoc}[2014/09/29]% v2.0u
\usepackage{xcolor}[2007/01/21]% v2.11
\definecolor{darkgreen}{rgb}{0.0, 0.3, 0.0}%
\usepackage{holtxdoc}[2012/03/21]%  v0.24
%% pagecolor may work with earlier versions of LaTeX2e and those
%% class and package, but this was not tested.
%% Please consider updating your LaTeX, class, and package
%% to the most recent version (if they are not already the most
%% recent version).
\hypersetup{%
 pdfsubject={Providing the thepagecolor command (HMM)},%
 pdfkeywords={LaTeX, pagecolor, thepagecolor, H.-Martin Muench},%
 pdfencoding=auto,%
 pdflang={en},%
 breaklinks=true,%
 linktoc=all,%
 pdfstartview=FitH,%
 pdfpagelayout=OneColumn,%
 bookmarksnumbered=true,%
 bookmarksopen=true,%
 bookmarksopenlevel=3,%
 pdfmenubar=true,%
 pdftoolbar=true,%
 pdfwindowui=true,%
 pdfnewwindow=true%
}
\CodelineIndex
\hyphenation{printing docu-ment}
\gdef\unit#1{\mathord{\thinspace\mathrm{#1}}}%
\begin{document}
  \DocInput{pagecolor.dtx}%
\end{document}
\endinput
%%
%% End of file `pagecolor.drv'.
